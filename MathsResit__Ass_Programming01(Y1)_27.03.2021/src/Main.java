import org.junit.runner.JUnitCore;
import java.util.ArrayList;
import org.junit.runner.Result;
import org.junit.runner.notification.Failure;

class Main
{
    public static void main(String[] args)
    {
      Result result = JUnitCore.runClasses(TestLogic.class);
		
      for (Failure failure : result.getFailures()) {
         System.out.println(failure.toString());
      }
		
      System.out.println(result.wasSuccessful());
      
      double[] thing = {2, 1, 3, 2, 4};
      double[] thing2 = {129, 102, 103, 10, 109};
      
      System.out.println("The thing is " + twoSampleTTest(thing, thing2, 10));
      System.out.println("Here " + groupMean(thing));
      
      /*
       * Hi , i could figure out what is was doing wrong with this i think
       * i screwed up some of the for loops but not sure, being working on this
       * for a few weeks but ran out of time, i just hope with some of the questions
       * their will be marks for getting the right formula :))
       */
    }

    /**
    * This function accepts one binary string (a String containing only the
    * characters '0' and '1') and returns the two's compliment of the binary
    * number. Represent the final number using the same number of bits as the
    * input string.
    * @param a - The binary number
    * @return A String representing the two's compliment of the argument.
    **/
    static String twosCompliment(String a)
    {
    	//NTS COMEBACK TOO ... (try making an array list maybe that is why i get the [] brackets)
    	//NTS numbers reversed again
    	
    	
    	String[] splita = a.split("");
    	String answer = null;
    	
    	//check to make sure that they all are not 0000 ....
    	if(doublecheck(splita) == true) {
    		answer = a;
    	}else {
    		
    		//switching the values (NOT WORKING)
    		for(int i = 0; i < splita.length; i++) {
    			if(splita[i] == "1") {
    				splita[i] = "0";
    			}else {
    				splita[i] = "1";
    			}
    		}
    		
    		//adding the one at the end
    		boolean bool = false;
    		for(int i = splita.length; bool == true || i == 0; i--) {
    			if(splita[i] == "0") {
    				splita[i] = "1";
    				bool = true;
    			}
    		}
    		
    		
    		 //putting it back into a string
    		for(int i = 0; i < splita.length; i++) {
    			answer += splita[i];
    		}
    	}
    	
    	
        return answer;
    }

    private static boolean doublecheck(String[] splita) {
		
    	boolean answer = true;
    	for(int i = 0; i < splita.length; i++) {
    		if(splita[i] == "1" ) {
    			answer = false;
    		}
    	}
    	
    	
		return answer;
	}

	/**
    * Identify if someone's IQ is statistically sigificantly above the mean for the
    * population, using the values provided to the function.
    * @param iq - individual's IQ
    * @param mean - population mean
    * @param sd - population standard deviation
    * @return true if iq significantly above the mean
    **/
    static boolean hasAboveAverageIQ(double iq, double mean, double sd)
    {
    	//NTS CHECKED ... ( don't know why failing last test passed the other two )
    	boolean answer = false;
    	double value;
    	
    	
    	//z score ( witin two standard dev )
    	value = iq - mean;
    	value = value/sd;
    	
    	if (value > 2) {
    		answer = true;
    		
    	}
        return answer;
    }

    /**
    * Identify if the average IQ of a group is statistically significantly different
    * to the mean of the population, using the values provided to the function.
    * @param group - array of iq scores of group
    * @param mean - population mean
    * @param sd - population standard deviation
    * @return true if mean iq of group is significant
    **/
    static boolean hasSignificantIQGroup(double[] group, double mean, double sd)
    {
    	//NTS DOUBLE CHECK THIS ... ( dont know what is happening here check again later )
    	boolean answer = false;
    	double groupMean = groupMean(group);
    	double part1, part2;
    	
    	part1 = groupMean - mean;
    	
    	part2 = sd / Math.sqrt(group.length);
    	
    	if ((part1/part2) > 2) {
    		answer = true;
    	}
    	
    	
        return answer;
    }

    /**
    * Perform a two-sample t-test.
    * @param group1 - array of scores
    * @param group2 - array of scores
    * @param expectedMeanDifference - expected difference between group means
    * @return t statistic
    **/
    static double twoSampleTTest(double[] group1, double[] group2, double expectedMeanDifference)
    {
    	//NTS CHECKK .... ( working but coming out with a slightly wrong answer double check )
    	double answer;
    	double size1 = group1.length;
    	double size2 = group2.length;
    	answer = (groupMean(group1) - groupMean(group2)) - expectedMeanDifference;
    	
    	//NTS tried to do the student two sample t-test here not sure if it works yet
    	double one = Math.pow(standardd(group1), 2)/size1;
    	double two = Math.pow(standardd(group2), 2)/size2;
    	
    	double part2 = one + two;
    	
    	part2 = Math.sqrt(part2);
    	
    	
        return answer/ part2;
    }

    /**
    * Finds the product-moment correlation co-efficient for two arrays of correlated data
    * @param x - x values
    * @param y - y values
    * @return the calculated pearson's r 
    **/
    static double pearsonsR(double[] x, double[] y)
    {
    	//NTS NOT WORKING COME BACK TOO ..... ( coming out with not a number )
    	double r = 0;
    	double part1 = 0, part2 = 0;
    	double sizex = x.length, sizey = y.length;
    	
    	double tx, ty;
    	for(int i = 0; i < sizex; i++) {
    		tx = x[i] - groupMean(x);
    		ty = y[i] - groupMean(y);
    		part1 = tx * ty;
    	}
    	
    	for(int i = 0; i < sizex; i++) {
    		tx = Math.pow(x[i] - groupMean(x), 2);
    		ty = Math.pow(y[i] - groupMean(y), 2);
    		part2 = Math.sqrt(tx * ty);
    	}
    	
    	
        return (part1/part2);
    }

    /**
    * Updates a running mean.
    * @param currentMean - The mean found by averaging the first `n` numbers of a stream
    * @param newNumber - The next number to include in the mean
    * @param n - the number of numbers averaged so far in the mean
    * @return the updated mean
    **/
    static double updateMean(double currentMean, double newNumber, int n)
    {
    	//NTS WORKING ... however 0.05 off remember to look into see if their is a 
    	//NTS different why of doing this ...
    	
    	ArrayList<Double> values = new ArrayList<Double>();
    	
    	//NTS don't think this is getting done properly ...
    	for(int i = 1; i < n; i++) {
    		values.add(currentMean);
    	}
    	values.add(newNumber);
    	
        return (groupMean(values));
    }

    private static double groupMean(ArrayList<Double> num) {
    	double answer = 0;
    	double size = num.size();
    	
    	for(int i = 0; i < size; i++) {
    		answer = answer + num.get(i);
    	}
    	answer = answer / size;
    		
    	return answer;
	}

	/**
    * Calculate the weighted average for an input vector with a vector of weights.
    * @param inputVector - Array of numbers to average
    * @param weights - Weights associated with each value in the input vector
    * @return weighted average plus bias
    **/
    static double weightedAverage(double[] inputVector, double[] weights)
    {
    	double answer = 0;
    	int size = inputVector.length;
    	double bias = 0;
    	
    	for(int i = 0; i < size; i++) {
    		answer =+ ((inputVector[i]) * (weights[i]));
    	}
    	
    	return answer;
    }

    /**
    * Implements the sigmoid activation function described above.
    * @param z - input to the sigmoid function
    * @return output of the sigmoid function
    **/
    static double sigmoid(double z)
    {
    	//NTS CHECKED ...
    	double answer;
    	double e = Math.exp(-(z));
    	answer = e + 1;
    	
    	
        return 1/answer;
    }

    /**
    * Returns the activation of a neuron with the given input vector, weights, and bias
    * @param inputVector - Array of numbers to average
    * @param weights - Weights associated with each value in the input vector
    * @param bias - Bias factor for neuron
    * @return the activation of the neuron
    **/
    static double neuronActivation(double[] inputVector, double[] weights, double bias)
    {
    	//NTS LOOK into this one ...
    	double answer = 0;
    	int size = inputVector.length;
    	
    	//NTS the array values are not coming though look into 
    	for(int i = 0; i < size; i++) {
    		answer =+ ((inputVector[i]) * (weights[i]));
    	}
    	
    	//adding the bais not sure if it is suppose to be in their but added it anyway...
    	answer =+ bias;
        return answer;
    }


    /**
    * Calculates the probability that a random ball will break if dropped a given number of times,
    * using the probabilities from the question above.
    * @param n - number of times ball is dropped
    * @return probability ball will be broken after n drops.
    **/
    static double simulateNDrops(int n)
    {
    	//NTS COME BAKC TOO ... 
    	double a = 0;
    	
    	if(10 >= Math.random()* 100 ) {
    		//if got a defective ball
    		
    		for(int i = 0; i < n; i++) {
    			a = a * 0.5;
    		}
    		
    		a = a * 0.1;
    	}else {
    		//if got a normal ball
    		
    		for(int i = 0; i < n; i++) {
    			a = a * 0.20;
    		}
    		
    		a = a * 0.9;
    	}
    	
    	
        return a;
    }
    
    static double groupMean(double[] num) {
    	double answer = 0;
    	double size = num.length;
    	
    	for(int i = 0; i < size; i++) {
    		answer += num[i];
    	}
    	
    		
    	return answer/size;
    }
    
    static double standardd(double[] num) {
		double answer = 0;
		double size = num.length;
		double sd = 0;
		
		for(double thing : num) {
			answer += thing;
		}
		
		double mean = answer/size;
		
		for(double thing : num) {
			sd += Math.pow(thing - mean, 2);
		}
		
    	
    	
    	return Math.sqrt(sd/size);
    }

}

