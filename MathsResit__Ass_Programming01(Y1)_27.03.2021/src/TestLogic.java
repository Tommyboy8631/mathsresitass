import static org.junit.Assert.*;  
import org.junit.Test;  

public class TestLogic {  

    @Test
    public void testTwosCompliment1()
    {
        assertEquals("1", Main.twosCompliment("1")); 
        assertEquals("0", Main.twosCompliment("0"));   
    }

    @Test
    public void testTwosCompliment2()
    {
        assertEquals("00", Main.twosCompliment("00")); 
        assertEquals("01", Main.twosCompliment("11"));   
        assertEquals("10", Main.twosCompliment("10"));   
        assertEquals("11", Main.twosCompliment("01"));   
    }

    @Test
    public void testTwosCompliment3()
    {
        assertEquals("000", Main.twosCompliment("000")); 
        assertEquals("111", Main.twosCompliment("001"));   
        assertEquals("110", Main.twosCompliment("010"));   
        assertEquals("101", Main.twosCompliment("011"));   
        assertEquals("100", Main.twosCompliment("100")); 
        assertEquals("011", Main.twosCompliment("101"));   
        assertEquals("010", Main.twosCompliment("110"));   
        assertEquals("001", Main.twosCompliment("111"));   
    }

    @Test
    public void testTwosCompliment8()
    {
        assertEquals("00000000", Main.twosCompliment("00000000")); 
        assertEquals("00000001", Main.twosCompliment("11111111")); 
        assertEquals("01101011", Main.twosCompliment("10010101")); 
        assertEquals("01000110", Main.twosCompliment("10111010")); 
    } 

    @Test
    public void testAboveAvIQ1()
    {
        assertEquals(false, Main.hasAboveAverageIQ(100, 100, 15)); 
        assertEquals(true, Main.hasAboveAverageIQ(200, 100, 15)); 
    } 

    @Test
    public void testAboveAvIQ2()
    {
        assertEquals(false, Main.hasAboveAverageIQ(123, 100, 15)); 
        assertEquals(true, Main.hasAboveAverageIQ(125, 100, 15)); 
    } 

    @Test
    public void testAboveAvIQ3()
    {
        assertEquals(false, Main.hasAboveAverageIQ(70, 100, 15)); 
        assertEquals(false, Main.hasAboveAverageIQ(50, 100, 15)); 
    } 

    @Test
    public void testSigIQGroup1()
    {
        assertEquals(true, Main.hasSignificantIQGroup(new double[] {70, 80}, 100, 15)); 
        assertEquals(true, Main.hasSignificantIQGroup(new double[] {150, 130}, 100, 15)); 
        assertEquals(false, Main.hasSignificantIQGroup(new double[] {95, 105}, 100, 15)); 
    } 

    @Test
    public void testSigIQGroup2()
    {
        assertEquals(false, Main.hasSignificantIQGroup(new double[] {90,95,85,80}, 100, 15)); 
        assertEquals(false, Main.hasSignificantIQGroup(new double[] {110,105, 115,120}, 100, 15)); 
        assertEquals(true, Main.hasSignificantIQGroup(new double[] {90,85,75,80}, 100, 15)); 
        assertEquals(true, Main.hasSignificantIQGroup(new double[] {110,115, 120,120}, 100, 15)); 
    } 

    @Test
    public void testTwoSampleT()
    {
        assertEquals(-1.608761, Main.twoSampleTTest(new double[] {301, 298, 295, 297, 304, 305, 309, 298, 291, 299, 293, 304}, new double[] {302, 309, 324, 313, 312, 310, 305, 298, 299, 300, 289, 294}, 0), 0.001); 
        assertEquals(1.915720, Main.twoSampleTTest(new double[] {150, 180, 190, 141, 150, 165 }, new double[] {140, 160, 130, 135, 155}, 0), 0.001); 
    } 

    @Test
    public void testPearsonsR()
    {
        assertEquals(0.5239, Main.pearsonsR(new double[] {12, 23,21,54,12,25,66}, new double[] {55, 120,90,300,55,50,80}), 0.001); 
        assertEquals(-0.364, Main.pearsonsR(new double[] {55,23,52,11,25}, new double[] {-32, -10, -50,-1, -78}), 0.001); 
    }

    @Test
    public void testUpdateMean()
    {
        assertEquals(10, Main.updateMean(10, 10, 9), 0.001);
        assertEquals(1.5, Main.updateMean(1, 6, 9), 0.001);
    }

    @Test
    public void testWeightedAverage()
    {
        assertEquals(1, Main.weightedAverage(new double[] {1}, new double[] {1}), 0.001);
        assertEquals(2, Main.weightedAverage(new double[] {1,1,1}, new double[] {1,2,3}), 0.001);
        assertEquals(2, Main.weightedAverage(new double[] {1,2,3}, new double[] {1,1,1}), 0.001);
        assertEquals(0.145783333333333, Main.weightedAverage(new double[] {0.2, 0.64, 0.47, 0.28, 0.15, 0.96 }, new double[] {0.17, 0.16, 0.78, 0.63, 0.15, 0.18 }), 0.001);
    }

    @Test
    public void testSigmoid()
    {
        assertEquals(0.7310585786300049, Main.sigmoid(1), 0.001);
        assertEquals(0.5, Main.sigmoid(0), 0.001);
        assertEquals(0.6224593312018546, Main.sigmoid(0.5), 0.001);
        assertEquals(0.26894142137, Main.sigmoid(-1), 0.001);
        assertEquals(0.377540668798, Main.sigmoid(-0.5), 0.001);
    }

    @Test
    public void testActivation()
    {
        assertEquals(0.897509169409567, Main.neuronActivation(new double[] {0.2, 0.64, 0.47, 0.28 }, new double[] {0.17, 0.16, 0.78, 0.63}, 2), 0.001);
    }

    @Test
    public void testDrops()
    {
        assertEquals(0.9999999999955427, Main.simulateNDrops(100), 0.00000000001);
        assertEquals(0.9975493195911682, Main.simulateNDrops(23), 0.00000000001);
    }
}  

